%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[english, aps, 12pt, prl]{revtex4}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{float}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage[colorlinks=true, linkcolor=black, citecolor=black]{hyperref}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{babel}
\usepackage{bm}
\usepackage{longtable}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand\dA{\bm {\dd A}}
\newcommand{\dd}{\mathrm{d}}
\newcommand{\Pa}{\mathrm{Pa}}
\newcommand{\kg}{\mathrm{kg}}
\newcommand{\m}{\mathrm{m}}
\newcommand{\mdot}{\bm{\dot{m}}}
\newcommand\jflux{\bm {j}}
\newcommand{\ux}{u_{x}}
\newcommand{\uy}{u_{y}}
\newcommand{\uz}{u_{z}}
\newcommand{\T}{\bm{T}}
\newcommand{\uu}{\bm{u}}
\newcommand{\UU}{\bm{U}}
\newcommand{\area}{\mathcal{S}}
\newcommand\qvec{\bm {q}}
\newcommand\evec{\bm {e}}
\newcommand\svec{\bm {s}}
\newcommand\matA{\bm {A}}
\newcommand\matL{\bm {\Lambda}}
\newcommand\V{\mathcal{V}}
\newcommand\poro{\varepsilon}
\newcommand\g{\bm {g}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\makeatletter
\providecommand{\tabularnewline}{\\}
\makeatother

\begin{document}
%%
\title{{\Large Clarificação do Mosto}\\ Trabalho 06 \\ TEFT 2015 --  PGMEC/UFF}

\author{Leandro Santos de Barros}
\affiliation{lsbarros@id.uff.br}
%
\author{Heitor Herculano}
\affiliation{heitorhb@yahoo.com.br }

%\date{\today}

\begin{abstract}
Utilizando um modelo de Darcy para escoamento em meio poroso (equação de {\it momentum}), obtenha  uma {\bf formulação matemática} para o transporte de massa por convecção com dois componentes (água pura e substâncias dissolvidas, essas últimas modeladas como um único componente) para o processo de clarificação do mosto. Assuma que os grãos são inertes e já estão depositados no fundo da tina de filtração no início do processo e que tudo ocorra à temperatura constante. Assuma também que o nível de água (e o resto do mosto) são mantidos constantes no topo da tina e que o processo é feito com fundo falso de modo que um modelo unidimensional transiente $(z,t)$ possa ser utilizado. Forneça todas as {\bf condições de contorno}. A solução numérica é opcional mas vale alguns pontos extras.
\end{abstract}
%%%
\maketitle
%%%
\newpage
{\bf Descrição do problema e hipóteses simplificadoras:} O escoamento de água e outras substâncias dissolvidas será modelado como um escoamento binário (dois componentes), em um meio poroso de matriz rígida (grãos inertes, altura $l_2$ constante, ver figura \ref{fig:volumedecontrole}). Será tratado como escoamento forçado do fluido devido ao gradiente de pressão com transporte de massa devido ao gradiente de concentração. A altura da coluna d'água $l_1$ é constante. Como será considerado o meio isotérmico, o modelo matemático inclui a equação de conservação de massa das espécies envolvidas e a equação de conservação de momento linear para meio poroso.
%
\begin{figure}[H]
\centering
\includegraphics[width=0.4\textwidth]{figuras/vc.pdf}
\caption{Ilustração do problema}
\label{fig:volumedecontrole}
\end{figure}
%
%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%

\vspace{1cm}
{\bf Definições:} A porosidade $\varepsilon$ é definida como a razão entre o volume disponível dentro da matriz porosa e o volume total. A massa específica da espécie A é definida como a razão entre a massa da espécie A e o volume disponível dentro da matriz porosa. A velocidade e massa específica médias do escoamento são definidas como:
%
\begin{subequations}
\begin{gather}\label{eq:def}
\rho = \rho_A + \rho_B, \\
\rho_{A,B} = M_{A,B}/(\varepsilon \V),\\
\rho\,\uu = \rho_A \uu_A + \rho_B \uu_B
\end{gather}
\end{subequations}
%%%%%%%%%%%%%%%%%%%%%%%%
%\vspace{0.5cm}
\newpage
{\bf Nomenclatura:} 

\noindent
\begin{longtable}[l]{p{50pt} p{60pt} p{300pt}}
\textbf{Símbolo}	& \textbf{Unidade} & \textbf{Descrição} \\
$c_s$        &kg/m$^3$& concetração de soluto;\\
$c_a$        &kg/m$^3$& concentração de água;\\
$D_{AB}$ &m$^2$/s  & coeficiente de difusão de massa para as espécies A e B;\\
$m_{i}$     &$-$& fração mássica da espécie `` i '' no volume de controle;\\
$M_{i}$ &kg& massa da espécie `` i '' no volume de controle;\\
$p$ &Pa& pressão;\\
$\V$ &m$^3$& volume de controle;\\
$z$   & m& direção vertical;\\
$t$  &s& tempo;\\
$\gamma$ &N/m$^3$& peso específico;\\
$\rho$ &kg/m$^2$& massa específica da mistura água e soluto;\\
$\poro$ &$-$& porosidade.\\
%...
\end{longtable}
%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{0.5cm}
{\bf Lei de Darcy:} Em 1856, Henry Darcy investigou o escoamento de água em filtro de areia vertical. A figura \ref{fig:darcy} mostra o aparato experimental usado por ele \cite{8}. Com base nos seus experimentos, Darcy concluiu que a taxa de escoamento (volume por unidade de tempo) $Q$ é proporcional a área de seção transversal $A$ onde o fluido escoa, proporcional a diferença de alturas ($h_1 - h_2$), e inversamente proporcional ao comprimento da matriz porosa $L$ na direção do escoamento. As conclusões anteriores levaram a famosa fórmula de Darcy:
%
\begin{gather}\label{eq:darcyOLD}
Q = K A (h_1-h_2)/L,
\end{gather}
onde $K$ é o coeficiente de proporcionalidade que depende, entre outras coisas, do material do meio poroso. A equação anterior foi generalizada da seguinte forma \cite{6}, \cite[cap. 4]{9}:
% texto. A equação \ref{eq:darcy} é  a lei de Darcy para escoamento laminar \cite{6}, %\cite[ver][cap.6, p. 6.29]{7}.
%%
\begin{gather}\label{eq:darcy}
\uu = -\frac{K}{\mu}(\nabla p - \rho\,\g),\\
\g = -g\,\evec_z
\end{gather}
%
\begin{figure}[H]
\centering
\includegraphics[width=0.6\textwidth]{figuras/Darcy.jpg}
\caption{Esquema do aparato experimental de Darcy \cite{4, 8}}
\label{fig:darcy}
\end{figure}
%
%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{0.5cm}
{\bf Lei de Fick:} Considere um sistema composto de duas substâncias (A e B), cuja superfície de controle é fixa e onde não existe um gradiente de temperatura (nem de pressão) apreciáveis.  Haverá migração de partículas da substância A (e também da B) caso não estejam distribuídas de forma homogênea no sistema. Para o cálculo do fluxo de massa faz-se uso da lei de Fick que estabelece que a migração de partículas é proporcional ao gradiente de concentração. Mais especificamente, temos que a taxa de massa transferida da espécie A, por unidade de tempo, por unidade de área perpendicular à direção da transferência, é proporcional à massa específica da mistura, $\rho = \rho_A + \rho_B$, e ao gradiente da fração mássica da espécie, $m_A = \rho_A/\rho$ \cite{2}. A equação \eqref{eq:Fick} é a forma vetorial da lei de Fick na base mássica:
%%
\begin{gather}\label{eq:Fick}
\jflux_A = - \rho D_{AB}\,\nabla\,m_A,
\end{gather}
entretanto a forma mais usada da lei de Fick, válida para escoamento de misturas diluídas,  é \cite{10}:
\begin{gather}\label{eq:Fick2}
\jflux_A = - D_{AB}\,\nabla \rho_A,
\end{gather}

%\begin{gather}\label{}
%\frac{\partial  \rho_\alpha}{\partial t} + \nabla \cdot (\rho_\alpha  \UU_\alpha) = %\mdot_\alpha,
%\end{gather}
%
%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%
{\bf Conservação de Massa:} %Texto texto  texto texto  texto texto  texto texto  texto texto  texto texto  texto texto  texto texto  texto texto  texto texto  texto texto  texto texto  texto  texto.A equação \ref{eq1} é a conservação de massa \cite[ver][cap. 1, p. 2]{3} e \cite[ver][p. 44]{4}. Me parece que a referência \cite[ver][p. 41]{5} é a mais organizada.
Considere  a massa total no sistema $M$, $V(t)$  o volume do sistema no tempo $t$ e $\partial V(t)$ a superfície de $V(t)$. A seguir a expressão para a massa do sistema em função da massa específica da mistura $\rho$, onde $\varepsilon$ é a porosidade do meio e a soma é realizada no volume disponível para o fluido escoar pelos poros da matriz rígida:
\begin{gather}\label{eq:massa1}
M = \int_{V(t)}\,\rho\, \varepsilon \dd V 
\end{gather}

Para se obter a forma diferencial da equação de conservação de massa, será de utilidade o teorema do transporte de Reynolds \cite{1}. Reescrevendo o balanço de conservação de massa  em uma  formulação de {\it volume de controle}, temos:
\begin{gather}\label{eq:massaABS}
\frac{\dd M}{\dd t} =
\int_{V(t)}\frac{\partial (\rho \varepsilon)}{\partial t} \dd V +
\int_{\partial V(t)}  (\rho \varepsilon)\,\uu \cdot \dA = 0,
\end{gather}
onde $\uu$ é a velocidade mássica média da mistura em meio poroso.  Fazendo uso do teorema de Gauss, temos que a equação \eqref{eq:massaABS} torna-se:
\begin{gather}
\int_{V(t)}\frac{\partial (\rho \varepsilon)}{\partial t} \dd V +
\int_{ V(t)}  \nabla (\rho \varepsilon \,\uu) \cdot\dd V = 0,
\end{gather}
e onde finalmente chegamos na sua forma final, para o problema unidimensional:
\begin{gather}\label{eq:massaABS2}
\frac{\partial \rho}{\partial t} +
\frac{\partial (\rho  \, u)}{\partial z} = 0
\end{gather}
%%%%%%

A seguir consideraremos o balanço de massa para a espécie $A$ (soluto, por exemplo). Considere $M_A$ a massa da espécie $A$ no sistema. A seguir temos a expressão para a massa em função da massa específica e a porosidade do meio:
\begin{gather}\label{eq:massa1}
M_A = \int_{V(t)}\rho_A \varepsilon \dd V 
\end{gather}

Pelo princípio de conservação de massa, temos, aplicando o teorema do transporte de Reynolds,  que:
\begin{gather}\label{eq:massaA}
\frac{\dd M_A}{\dd t} =
\int_{V(t)}\frac{\partial (\rho_A \varepsilon)}{\partial t} \dd V +
\int_{\partial V(t)}  (\rho_A \varepsilon)\,\uu_A \cdot \dA = 0,
\end{gather}
onde $\uu_A$ é a velocidade absoluta da espécie $A$. Definindo a velocidade mássica média como $\rho\,\uu = \rho_A \uu_A + \rho_B \uu_B$, podemos definir o {\it fluxo mássico da espécie $A$ em relação à velocidade mássica média da mistura em meio poroso} como \cite{2}:
%
%
\begin{gather}\label{eq:massa1}
\jflux_A \equiv \rho_A (\uu_A - \uu)
\end{gather}
%
%

Reconhecendo a expressão anterior na equação \eqref{eq:massaA}, temos:
%
%
\begin{gather}\label{eq:massaA2}
\int_{V(t)}\frac{\partial (\rho_A \varepsilon)}{\partial t} \dd V +
\int_{\partial V(t)}  (\rho_A \varepsilon)\,\uu \cdot \dA = - \int_{\partial V(t)}  (\rho_A \varepsilon)\,(\uu_A -\uu) \cdot \dA,
\end{gather}
%
%
logo, usando o teorema de Gauss e a lei de Fick, temos:
%
%
\begin{subequations}
\begin{align}\label{eq:massaA3}
\int_{V(t)}\frac{\partial (\rho_A \varepsilon)}{\partial t} \dd V +
\int_{ V(t)} \nabla\cdot (\rho_A \varepsilon \uu) \dd V &= - \int_{\partial V(t)} \varepsilon \jflux_A \cdot \dA \\
&=   \int_{\partial V(t)} \varepsilon D_{AB} \nabla \rho_A \cdot \dA ,\\
&=   \int_{ V(t)} \nabla \cdot (\varepsilon D_{AB} \nabla \rho_A)  \dd V ,
\end{align}
\end{subequations}
%
%
a expressão anterior pode ser escrita na sua versão local, como:
%
%
\begin{gather}\label{eq:massaA4}
\frac{\partial (\rho_A \varepsilon)}{\partial t} +
\nabla\cdot (\rho_A \varepsilon \uu)  =   \nabla \cdot (\varepsilon D_{AB} \nabla \rho_A),
\end{gather}
%
%
e finalmente a sua versão unidimensional, supondo porosidade e coeficiente de difusão constantes:
%
%
\begin{gather}\label{eq:massaA5}
\frac{\partial \rho_A }{\partial t} +
\frac{\partial( \rho_A  u)}{\partial z}  =  D_{AB} \frac{\partial^2 \rho_A}{\partial z^2},
\end{gather}
%
%

%
%%%%%%%%%%%%%%%%%%%%%%%%
%
%
%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{0.5cm}
{\bf Resumo das equações de governo:} As equações que regem o problema (\ref{eq:darcy}, \ref{eq:massaABS2}, \ref{eq:massaA5}) são  válidas para $0 < z <  l_{2}$. A seguir um resumo das equações:
%
%
\begin{subequations}
\begin{align}
u &= -\frac{K}{\mu}\Big(\frac{\partial p}{\partial z} + g\Big),
\\
\frac{\partial \rho}{\partial t} +
\frac{\partial (\rho  \, u)}{\partial z} &= 0,
\\
\frac{\partial \rho_A }{\partial t} +
\frac{\partial( \rho_A  u)}{\partial z}  &=  D_{AB} \frac{\partial^2 \rho_A}{\partial z^2},
\end{align}
\end{subequations}
%
%
e  as condições de contorno do problema estão a seguir. Foi usada uma condição de taxa de variação nula na saída ($z=0$) para a massa específica de soluto ($\rho_A$).
%
%
\begin{subequations}
  \begin{align}
	& u = -\frac{K}{\mu}\Big(\frac{\Delta p}{l_2} + g\Big),\\
         & \rho_A = 0, \quad p = \Delta p, & \text{ at } z = l_2
	\label{eq5}
	\\
	& p = 0, \quad \frac{\partial \rho_A}{\partial z} = 0, & \text{ at } z = 0 
	\label{eq6}
  \end{align}
\end{subequations}
%
%
%%%%%%%%%%%%%%%%%%%%%%%%
\newpage
\hspace{6.4cm} {\bf  Referências} 
\begin{thebibliography}{11}
\bibitem{1} Kundu, Pijush. ``Fluid Mechanics''. Academic Press, 2$^{th}$ edition (2002)

\bibitem{2}Incropera, DeWitt, Berman e Lavine. ``Fundamentos de Transferência de Calor e de Massa''. LTC (2008)

%\bibitem{3} Jansen, Jan Dirk. ``A Systems Description of Flow Through Porous Media ''. New York: Springer (2013)

\bibitem{4}  Bear, Jacob. `` Dynamics of Fluids in Porous Media''. New York: Dover (1988)

%\bibitem{5} Nield, Donald A., and Adrian Bejan. ``Convection in Porous Media
%''. Springer (2006)

\bibitem{6} M. King Hubbert. ``Darcy's Law and the Field Equations of the Flow of Undergrounds Fluids'', International Association of Scientific Hydrology. Bulletin, 2:1, 23-59 (1957) 

%\bibitem{7} Thomas W. Engler. ``Fluid Flow in Porous Media''. (2010)

\bibitem{8} Darcy, H. P. G. ``Dètermination des lois d'ècoulement de l'eau à travers le sable.'' (1856)

\bibitem{9} Whitaker, Stephen. ``The Method of Volume Averaging. '' Vol. 13, Springer Science \& Business Media. (1999)

\bibitem{10} Cussler, Edward Lansing. ``Diffusion: Mass Transfer in Fluid Systems.'' Cambridge University Press (2009)
\end{thebibliography}
%%%%%%%%%%%%%%%%%%%%%%%%
%
%
\end{document}
